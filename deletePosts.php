<?php

require_once "Classes/DataBase/Products.php";

use Classes\DataBase\Products as Products;

$successMessage = "Post Not Deleted";
$data = new Products("junior_test");
$productsData = $data->dbselect("products");

$checkID = array();
$typeTableName = array();
foreach ($productsData as $post) {
    if (isset($_POST["post_delete_" . $post["ID"]])) {
        array_push($checkID, $post["ID"]);
        array_push($typeTableName, $_POST["post_tableName_" . $post["ID"]]);
    }
}

// deleting
if (count($checkID) == 0) $successMessage = "if you want to delete a record, at first you need to check it";
for ($i = 0; $i < count($checkID); $i++) {
    $id = $checkID[$i];
    $tableName = $typeTableName[$i];
    if ($data->dbdelete($tableName, "Product_ID", $id) && $data->dbdelete("products", "ID", $id)) {
        $successMessage = "Post Deleted";
    }
}

echo $successMessage . "\n";