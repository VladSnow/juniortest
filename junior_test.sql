-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1
-- Время создания: Ноя 18 2018 г., 17:22
-- Версия сервера: 10.1.36-MariaDB
-- Версия PHP: 7.2.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `junior_test`
--

-- --------------------------------------------------------

--
-- Структура таблицы `products`
--

CREATE TABLE `products` (
  `ID` int(10) UNSIGNED NOT NULL,
  `SKU` varchar(50) NOT NULL,
  `Name` varchar(100) NOT NULL,
  `Price` float NOT NULL,
  `TypeSwitcher_ID` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `products`
--

INSERT INTO `products` (`ID`, `SKU`, `Name`, `Price`, `TypeSwitcher_ID`) VALUES
(9, 'JVC 1', 'Acme DISC', 1, 1),
(10, 'JVC 2', 'Acme DISC', 2, 1),
(11, 'JVC 3', 'Acme DISC', 3, 1),
(12, 'JVC 4', 'Acme DISC', 4, 1),
(21, 'JVC 6', 'Blu-ray', 8.5, 1),
(22, 'GGW 1', 'War and Peace', 20, 2),
(23, 'TR 1', 'Chair', 40, 3),
(30, 'SKU', 'NAME', 56, 1),
(31, 'SKU', 'NAME 2', 45.89, 1),
(32, 'sku', 'name', 300, 1),
(33, 'sku', 'namamam', 300, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `type_dimensions`
--

CREATE TABLE `type_dimensions` (
  `ID` int(10) UNSIGNED NOT NULL,
  `Product_ID` int(10) UNSIGNED NOT NULL,
  `height` int(10) NOT NULL,
  `width` int(10) NOT NULL,
  `length` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `type_dimensions`
--

INSERT INTO `type_dimensions` (`ID`, `Product_ID`, `height`, `width`, `length`) VALUES
(1, 23, 24, 45, 15);

-- --------------------------------------------------------

--
-- Структура таблицы `type_size`
--

CREATE TABLE `type_size` (
  `ID` int(10) UNSIGNED NOT NULL,
  `Product_ID` int(11) UNSIGNED NOT NULL,
  `size` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `type_size`
--

INSERT INTO `type_size` (`ID`, `Product_ID`, `size`) VALUES
(2, 9, 700),
(3, 10, 750),
(4, 11, 800),
(5, 12, 900),
(6, 21, 600),
(8, 30, 700),
(9, 31, 800),
(10, 32, 400),
(11, 33, 400);

-- --------------------------------------------------------

--
-- Структура таблицы `type_switcher`
--

CREATE TABLE `type_switcher` (
  `ID` int(5) UNSIGNED NOT NULL,
  `type` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `type_switcher`
--

INSERT INTO `type_switcher` (`ID`, `type`) VALUES
(1, 'size'),
(2, 'weight'),
(3, 'dimensions');

-- --------------------------------------------------------

--
-- Структура таблицы `type_weight`
--

CREATE TABLE `type_weight` (
  `ID` int(10) UNSIGNED NOT NULL,
  `Product_ID` int(11) UNSIGNED NOT NULL,
  `weight` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `type_weight`
--

INSERT INTO `type_weight` (`ID`, `Product_ID`, `weight`) VALUES
(1, 22, 2);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `TypeSwitcher_ID` (`TypeSwitcher_ID`);

--
-- Индексы таблицы `type_dimensions`
--
ALTER TABLE `type_dimensions`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `Product_ID` (`Product_ID`);

--
-- Индексы таблицы `type_size`
--
ALTER TABLE `type_size`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `type_size_ibfk_1` (`Product_ID`);

--
-- Индексы таблицы `type_switcher`
--
ALTER TABLE `type_switcher`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `ID` (`ID`) USING BTREE;

--
-- Индексы таблицы `type_weight`
--
ALTER TABLE `type_weight`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `Product_ID` (`Product_ID`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `products`
--
ALTER TABLE `products`
  MODIFY `ID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT для таблицы `type_dimensions`
--
ALTER TABLE `type_dimensions`
  MODIFY `ID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `type_size`
--
ALTER TABLE `type_size`
  MODIFY `ID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT для таблицы `type_switcher`
--
ALTER TABLE `type_switcher`
  MODIFY `ID` int(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT для таблицы `type_weight`
--
ALTER TABLE `type_weight`
  MODIFY `ID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_ibfk_1` FOREIGN KEY (`TypeSwitcher_ID`) REFERENCES `type_switcher` (`ID`) ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `type_dimensions`
--
ALTER TABLE `type_dimensions`
  ADD CONSTRAINT `type_dimensions_ibfk_1` FOREIGN KEY (`Product_ID`) REFERENCES `products` (`ID`) ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `type_size`
--
ALTER TABLE `type_size`
  ADD CONSTRAINT `type_size_ibfk_1` FOREIGN KEY (`Product_ID`) REFERENCES `products` (`ID`) ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `type_weight`
--
ALTER TABLE `type_weight`
  ADD CONSTRAINT `type_weight_ibfk_1` FOREIGN KEY (`Product_ID`) REFERENCES `products` (`ID`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
