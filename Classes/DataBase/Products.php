<?php

namespace Classes\DataBase;
use mysqli;

class Products
{
    public $connect;

    public function __construct($dbname)
    {
        $this->connect = new mysqli("localhost", "root", "", $dbname);
        if ($this->connect->connect_error) {
            echo "Connection failed: " . $this->connect->connect_error;
        }
    }

    public function dbselect($tableName) {
        $array = array();
        $query = "SELECT * FROM " . $tableName;
        $result = $this->connect->query($query);
        while ($row = $result->fetch_assoc()) {
            $array[] = $row;
        }
        return $array;
    }

    public function dbinsert($tableName, $data) {
        $query = "INSERT INTO ".$tableName." (" .
            implode(",", array_keys($data)) . ") VALUES (" .
            "'" . implode("','", array_values($data)) . "')";
        if ($this->connect->query($query)) {
            return true;
        } else {
            echo $this->connect->error;
            return false;
        }
    }

    public function dbdelete($tableName, $field, $idValue) {
        $query = "DELETE FROM " . $tableName . " WHERE " .
            $field . " = " . $idValue;
        if ($this->connect->query($query)) {
            return true;
        } else {
            echo $this->connect->error;
            return false;
        }
    }
    /**
     * this method returns an array of the required row from the table.
     */
    public function getItemRowArray($tableName, $field, $data) {
        $array = array();
        $query = "SELECT * FROM " . $tableName . " WHERE " . $field . " = '" . $data . "'";
        $result = $this->connect->query($query);
        while ($row = $result->fetch_assoc()) {
            $array[] = $row;
        }
        return $array;
    }
    /**
     * this method returns the id value of the last item in the database from the table: $tableName.
     */
    protected function getLastIdValue($tableName) {
        $arr = array();
        $queryID = "SELECT MAX(ID) AS ID FROM " . $tableName;
        $result = $this->connect->query($queryID);
        while ($row = $result->fetch_assoc()) {
            $arr[] = $row;
        }
        return $arr[0]["ID"];
    }
}