<?php

namespace Classes\DataBase;

use Classes\DataBase\Products as Products;
use Classes\DataBase\SpecialAttribute as SpecialAttribute;

class Dimensions extends Products implements SpecialAttribute
{
    private $tableName = "type_dimensions";
    const NEEDED_STATES = 6;

    public function insertAttributes($data) {
        $idValue = $this->getLastIdValue("products");
        $query = "INSERT INTO " . $this->tableName .
            " (Product_ID, height, width, length) VALUES (" . $idValue .
            "," . implode(",", array_values($data)) . ")";
        if ($this->connect->query($query)) {
            return true;
        } else {
            echo $this->connect->error;
            return false;
        }
    }
}