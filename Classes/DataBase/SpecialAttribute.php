<?php

namespace Classes\DataBase;


interface SpecialAttribute
{
    /**
     * this function adds the required data to the database of a special attribute.
     * such as: Size, Dimensions, Weight.
     */
    public function insertAttributes($data);
}