<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <title>Product Add</title>

    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="/JuniorTest/css/main.css">
    <!--  Bootstrap  -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
</head>
<body>
    <div class="wrapper_all">

<!--   HEADER     -->

        <div class="container  header">
            <div class="row">
                <div class="col-sm">
                    <h2>Product Add</h2>
                </div>
                <div class="col-auto">
                    <a class="btn btn-outline-primary  js-save-btn" href="#">Save</a>
                </div>
            </div>
        </div>

<!--    CONTENT    -->

        <div class="container">
            <form class="needs-validation  js-form" method="post" action="/JuniorTest/post.php" novalidate>
                <div class="js-result"></div>
                <div class="input-group mb-3 col-lg-4">
                    <div class="input-group-prepend">
                        <span class="input-group-text  content-title" id="inputGroup-sizing-default">SKU</span>
                    </div>
                    <input class="form-control  js-input-sku" type="text" name="post_sku" aria-label="Default" aria-describedby="inputGroup-sizing-default" required>
                    <div class="invalid-feedback">
                        Please provide a valid SKU.
                    </div>
                </div>
                <div class="input-group mb-3 col-lg-4">
                    <div class="input-group-prepend">
                        <span class="input-group-text  content-title" id="inputGroup-sizing-default">Name</span>
                    </div>
                    <input class="form-control  js-input-name" type="text" name="post_name" aria-label="Default" aria-describedby="inputGroup-sizing-default" required>
                    <div class="invalid-feedback">
                        Please provide a valid Name.
                    </div>
                </div>
                <div class="input-group mb-3 col-lg-4">
                    <div class="input-group-prepend">
                        <span class="input-group-text  content-title" id="inputGroup-sizing-default">Price</span>
                    </div>
                    <input class="form-control  js-input-price" type="text" name="post_price" aria-label="Default" aria-describedby="inputGroup-sizing-default" pattern="^[0-9.,]*$" required>
                    <div class="invalid-feedback">
                        Please provide a valid Price. (only number)
                    </div>
                </div>
                <div class="input-group mb-3 col-lg-4">
                    <div class="input-group-prepend">
                        <span class="input-group-text  content-title" id="inputGroup-sizing-default">Type Switcher</span>
                    </div>
                    <select class="custom-select  js-type-switcher" name="post_type">
                        <option value="size">Size</option>
                        <option value="dimensions">Dimensions</option>
                        <option value="weight">Weight</option>
                    </select>
                </div>

<!--    TYPE SWITCHERS    -->
<!--    TYPE: SIZE   -->

                <div class="content-col js-type-size content-display-none">
                    <div class="card">
                        <div class="card-body">
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text  content-title" id="inputGroup-sizing-default">Size</span>
                                </div>
                                <input class="form-control  js-input-size" type="text" name="post_size" aria-label="Default" aria-describedby="inputGroup-sizing-default" pattern="^[0-9.,]*$">
                                <div class="invalid-feedback">
                                    Please provide a valid Size. (only number)
                                </div>
                            </div>
                            <p class="card-text">
                                Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                                when an unknown printer took a galley of type and scrambled it to make a type specimen book.
                            </p>
                        </div>
                    </div>
                </div>

<!--    TYPE: DIMENSIONS     -->

                <div class="content-col js-type-dimensions content-display-none">
                    <div class="card">
                        <div class="card-body">
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text  content-title" id="inputGroup-sizing-default">Height</span>
                                </div>
                                <input class="form-control  js-input-dimensions js-input-dimension-h" type="text" name="post_height" aria-label="Default" aria-describedby="inputGroup-sizing-default" pattern="^[0-9.,]*$">
                                <div class="invalid-feedback">
                                    Please provide a valid Height. (only number)
                                </div>
                            </div>
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text  content-title" id="inputGroup-sizing-default">Width</span>
                                </div>
                                <input class="form-control  js-input-dimensions js-input-dimension-w" type="text" name="post_width" aria-label="Default" aria-describedby="inputGroup-sizing-default" pattern="^[0-9.,]*$">
                                <div class="invalid-feedback">
                                    Please provide a valid Width. (only number)
                                </div>
                            </div>
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text  content-title" id="inputGroup-sizing-default">Length</span>
                                </div>
                                <input class="form-control  js-input-dimensions js-input-dimension-l" type="text" name="post_length" aria-label="Default" aria-describedby="inputGroup-sizing-default" pattern="^[0-9.,]*$">
                                <div class="invalid-feedback">
                                    Please provide a valid Length. (only number)
                                </div>
                            </div>
                            <p class="card-text">
                                Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                                when an unknown printer took a galley of type and scrambled it to make a type specimen book.
                            </p>
                        </div>
                    </div>
                </div>

<!--    TYPE: WEIGHT    -->

                <div class="content-col js-type-weight content-display-none">
                    <div class="card">
                        <div class="card-body">
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text  content-title" id="inputGroup-sizing-default">Weight</span>
                                </div>
                                <input class="form-control  js-input-weight" type="text" name="post_weight" aria-label="Default" aria-describedby="inputGroup-sizing-default" pattern="^[0-9.,]*$">
                                <div class="invalid-feedback">
                                    Please provide a valid Weight. (only number)
                                </div>
                            </div>
                            <p class="card-text">
                                Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                                when an unknown printer took a galley of type and scrambled it to make a type specimen book.
                            </p>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4  js-special-attribute"></div>

            </form>
        </div>

    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script type="text/javascript" src="/JuniorTest/js/new.js"></script>
</body>
</html>