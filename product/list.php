<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <title>Product List</title>

    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="/JuniorTest/css/main.css">
<!--  Bootstrap  -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
</head>
<body>
    <div class="wrapper_all">

<!--   HEADER     -->

        <div class="container header">
            <div class="row">
                <div class="col-sm">
                    <h2>Product List</h2>
                </div>
                <div class="col-md-auto">
                    <div class="input-group">
                        <select class="custom-select">
                            <option value="delete">Mass Delete Action</option>
                        </select>
                        <div class="input-group-append">
                            <a class="btn btn-outline-primary  js-apply-btn" href="#">Apply</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

<!--    CONTENT    -->
        <form class="js-form" method="post" action="/JuniorTest/deletePosts.php">
            <div class="container">
                <?php
                $keyCounter = 0;
                foreach ($productsData as $key => $post):
                    $keyCounter++;
                     if ($key % 4 == 0) {
                ?>
                    <div class="row">
                    <?php
                    }
                    ?>
                        <div class="col-md-3  content-col">
                            <div class="card text-center">
                                <div class="card-body">
                                    <input class="position-absolute  content-checkbox" type="checkbox" name="post_delete_<?php echo $post["ID"]; ?>">
                                    <p class="card-text"><?php echo $post["SKU"]; ?></p>
                                    <p class="card-text"><?php echo $post["Name"]; ?></p>
                                    <p class="card-text"><?php echo number_format($post["Price"], 2, '.', '') . " $"; ?></p>
                                    <p class="card-text">
                                        <?php
                                        $typeSwitcher = $data->getItemRowArray("type_switcher", "ID", $post["TypeSwitcher_ID"]);
                                        $tableName = "type_" . $typeSwitcher[0]["type"];
                                        $tableID = $post["ID"];
                                        $typeData = $data->getItemRowArray($tableName, "Product_ID", $tableID);

                                        $baseUnit = array(
                                            "size" => "MB",
                                            "weight" => "KG",
                                            "dimensions" => ""
                                        );
                                        $size = count($typeData[0]) - 2;
                                        $count = 0;
                                        foreach ($typeData[0] as $keys => $value) {
                                            if ($keys != "ID" && $keys != "Product_ID") {
                                                if ($size > 1 && $count < $size - 1) {
                                                    echo $value . "x";
                                                } else {
                                                    echo $value . " " . $baseUnit[$typeSwitcher[0]["type"]];
                                                }
                                                $count++;
                                            }
                                        }
                                        ?>
                                    </p>
                                    <input class="content-display-none" type="text" name="post_tableName_<?php echo $post["ID"]; ?>" value="<?php echo $tableName; ?>">
                                </div>
                            </div>
                        </div>
                    <?php
                    if ($keyCounter == 4) {
                        $keyCounter = 0;
                    ?>
                    </div>
                <?php
                    }
                endforeach;
                $data->connect->close();
                ?>
            </div>
        </form>

    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script type="text/javascript" src="/JuniorTest/js/list.js"></script>
</body>
</html>