Junior Test v2.2
===
Directory **product/** contains 2 web pages: list.php, new.php

* **list.php** - main web page. Web page have all existing products list and details.
 It opens at startup **index.php**. If you want to delete a record, at first you need to check item
 and click *Apply*.
 
![product list page](README/image/product_list_page.png)

* **new.php** - web page to add a new item to the database. processes data *post.php.*

![product new page](README/image/product_new_page.png)

The web page works together with the database.
The file that you need to connect to the database: **junior_test.sql**.

**Used technologies:**

* PHP: ^7.0, OOP;
* JavaScript. jQuery;
* Bootstrap;
* MySQL: ^5.6;
* PSR-1/2;
* HTML/CSS;

> Create by **Vladislav Kapustjonok**