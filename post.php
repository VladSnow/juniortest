<?php

// Classes
require_once __DIR__."/Classes/DataBase/Products.php";
require_once __DIR__."/Classes/DataBase/SpecialAttribute.php";
require_once __DIR__."/Classes/DataBase/Size.php";
require_once __DIR__."/Classes/DataBase/Dimensions.php";
require_once __DIR__."/Classes/DataBase/Weight.php";

use Classes\DataBase\Size as Size;
use Classes\DataBase\Weight as Weight;
use Classes\DataBase\Dimensions as Dimensions;

// variables
$readyStatesArray = getReadyInputsDataArray();
$successMessage = "Post Not Inserted";
$postType = $_POST["post_type"];
$objNames = array(
    "size" => new Size("junior_test"),
    "dimensions" => new Dimensions("junior_test"),
    "weight" => new Weight("junior_test")
);

// object
$data = new $objNames[$postType]("junior_test");
$typeSwitcherID = $data->getItemRowArray("type_switcher", "type", $postType)[0]["ID"];

if (isReady($data::NEEDED_STATES, $readyStatesArray)) {
    $dataAttributes = getDataAttributes($readyStatesArray);
    $insertData = array(
        "SKU" => $_POST['post_sku'],
        "Name" => $_POST['post_name'],
        "Price" => $_POST['post_price'],
        "TypeSwitcher_ID" => $typeSwitcherID
    );
    if ($data->dbinsert("Products", $insertData) && $data->insertAttributes($dataAttributes)) {
        $successMessage = 'Post Inserted';
    }
    echo $successMessage . "\n";
} else {
    echo "need to fill in all fields";
}

/* FUNCTIONS */
/**
 * this function takes 2 parameters:
 * $states - the amount of necessary data to add to the database,
 * $data - and the array of data that is already done.
 */
function isReady($states, $data) {
    $count = count($data) - 1;
    return $count == $states ? true : false;
}
/**
 * this function get all data POST inputs
 * which is ready and not empty.
 */
function getReadyInputsDataArray() {
    $dataArray = array();
    foreach ($_POST as $post) {
        if (!empty($post)) {
            $dataArray[] = $post;
        }
    }
    return $dataArray;
}
/**
 * this function get all data POST inputs
 * which are only relevant for a special attribute.
 * ---
 * skips the first 3 input fields: SKU, Name, Price.
 */
function getDataAttributes($arr) {
    $dataTypeArr = array();
    foreach ($arr as $key => $post) {
        if ($key > 3) {
            $dataTypeArr[] = $post;
        }
    }
    return $dataTypeArr;
}