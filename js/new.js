$(document).ready(function() {

    changeSpecialAttribute();
    $('.js-type-switcher').on('change', function() {
        changeSpecialAttribute();
    });

    /* validation form */

    var isValid = false;
    window.addEventListener('load', function() {
        // Fetch all the forms we want to apply custom Bootstrap validation styles to
        var forms = document.getElementsByClassName('needs-validation');
        // Loop over them and prevent submission
        var validation = Array.prototype.filter.call(forms, function(form) {
            $(".js-save-btn").on('click', function() {
                isValid = (form.checkValidity() === false) ? false : true;
                form.classList.add('was-validated');

                // Form POST
                if (isValid) {
                    var $formClass = ".js-form";
                    $.ajax({
                        type: $($formClass).attr("method"),
                        url: $($formClass).attr("action"),
                        data: new FormData(document.querySelector($formClass)),
                        contentType: false,
                        cache: false,
                        processData: false,
                        success: function (data) {
                            alert(data);
                            location.reload();
                        }
                    });
                } else {
                    console.log("not Valid");
                }
            });
        });
    }, false);

    /* FUNCTIONS */
    /**
     * this function change div block ".js-special-attribute".
     * the form (special attribute) dynamically changed when
     * type is switched.
     */
    function changeSpecialAttribute() {
        var $type = $(".js-type-switcher").val(),
            $data = $(".js-type-" + $type).html(),
            $className = ".js-input-" + $type;
        $(".js-special-attribute").html($data);
        $(".js-special-attribute " + $className).prop("required", true);
    }
});