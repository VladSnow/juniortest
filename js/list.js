/**
 * JS file for list.php
 */

// click event. to process data in the file: deletePosts.php
$(".js-apply-btn").click(function() {
    var $formClass = ".js-form";
    $.ajax({
        type: $($formClass).attr("method"),
        url: $($formClass).attr("action"),
        data: new FormData(document.querySelector($formClass)),
        contentType: false,
        cache: false,
        processData: false,
        success: function (data) {
            alert(data);
            location.reload();
        }
    });
});
